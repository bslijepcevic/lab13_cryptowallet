﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {
    public class CryptoCurrency {
        public CryptoCurrency(string name, string kratica, decimal iznos) {
            Name = name;
            Kratica = kratica;
            Iznos = iznos;
        }

        public string Name { get; set; }
        public string Kratica { get; set; }
        public decimal Iznos { get; set; }
        public string BalanceHuman {
            get {
                return $"{Iznos} {Kratica}";
            }
        }
        public override string ToString() {
            return Name;
        }

    }
}
