﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Serijalizacija {
    public class Wallet {
        private List<CryptoCurrency>? currencies;

        public Wallet() {
            currencies = new List<CryptoCurrency>();
        }

        public void AddCurrency(CryptoCurrency currency) {
            currencies.Add(currency);
            Serialize();
        }

        public IEnumerable<CryptoCurrency> GetCurrencies() {
            return
              from c in currencies
              orderby c.Name
              select c;
        }

        public void Serialize() {
            string currenciesInJson = JsonSerializer.Serialize(currencies);
            File.WriteAllText("podaci.txt", currenciesInJson);
        }

        public void Deserialize() {
            string podaci = File.ReadAllText("podaci.txt");
            currencies = JsonSerializer.Deserialize<List<CryptoCurrency>>(podaci);
        }

    }
}
